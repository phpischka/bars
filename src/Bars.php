<?php

namespace Bars\Bars;

class Bars {
    
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
